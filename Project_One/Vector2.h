#pragma once

struct Vector2
{
	// coordinates
	long x, y;

	// ctor
	Vector2() : x(0), y(0) {}
	Vector2(long start_x, long start_y) : x(start_x), y(start_y) {}
	~Vector2() {}
};

static Vector2 ZeroVector(0, 0);