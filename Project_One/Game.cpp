#include "Game.h"
#include "GameState.h"
#include "win32_main.h"
#include "Console.h"

bool CGame::Init(HINSTANCE Instance, int CmdShow, std::string Title, bool fullscreen)
{


	WNDCLASSEX Window = { 0 };
	Window.cbSize = sizeof(WNDCLASSEX);
	Window.style = CS_HREDRAW | CS_VREDRAW;
	Window.lpfnWndProc = WindowProc;
	Window.cbClsExtra = 0;
	Window.cbWndExtra = 0;
	Window.hInstance = Instance;
	Window.hIcon = 0;
	Window.hCursor = LoadCursor(nullptr, IDC_CROSS);
	Window.hbrBackground = 0;
	Window.lpszMenuName = 0;
	Window.lpszClassName = "Win32Window";

	RegisterClassExA(&Window);

	HWND WindowHandle = CreateWindow(Window.lpszClassName, Title.c_str(),
		WS_OVERLAPPEDWINDOW | WS_VISIBLE,
		CW_USEDEFAULT, CW_USEDEFAULT,
		WINDOWWIDTH, WINDOWHEIGHT,
		0, 0, Instance, 0);

	ShowWindow(WindowHandle, CmdShow);
	UpdateWindow(WindowHandle);

	if (!D3D_Init(WindowHandle, fullscreen) || !Gamepad_Init())
	{
		OutputDebugStringA("Failed to init Direct3D Components");
		return false;
	}

	sHardware_Info = GetHardwareInfo();

	m_running = true;

	camera = { 0, 0, WINDOWWIDTH, WINDOWHEIGHT };

	console.Init(this);

	timeNow = timeGetTime();

	return true;
}

void CGame::Cleanup()
{
}

void CGame::ChangeState( CGameState* state )
{
	// cleanup the current state
	if ( !states.empty() ) 
	{
		states.back()->Cleanup();
		states.pop_back();
	}

	// store and init the new state
	states.push_back(state);
	states.back()->Init(this);
}

void CGame::PushState(CGameState* state)
{
	// pause current state
	if ( !states.empty() ) 
	{
		states.back()->Pause();
	}

	// store and init the new state
	states.push_back(state);
	states.back()->Init(this);
}

void CGame::PopState()
{
	// cleanup the current state
	if ( !states.empty() ) 
	{
		states.back()->Cleanup();
		states.pop_back();
	}

	// resume previous state
	if ( !states.empty() ) 
	{
		states.back()->Resume();
	}
}

void CGame::HandleEvents()
{
	states.back()->HandleEvents(this);
}

void CGame::Update()
{
	states.back()->Update(this);
}

void CGame::Draw()
{
	states.back()->Draw(this);
}

void CGame::DrawTextString(int x, int y, const char * text, DWORD color)
{
	RECT textBound = { x, y , 0, 0};
	sprite->Begin(D3DXSPRITE_ALPHABLEND);
	font->DrawText(sprite, text, -1, &textBound, DT_CALCRECT, color);
	font->DrawText(sprite, text, -1, &textBound, DT_LEFT | DT_NOCLIP, color);
	sprite->End();
}


bool CGame::D3D_Init(HWND hWnd, BOOL fullscreen)
{
	//TODO(jzb): replace message boxes with logs
	LPDIRECT3D9 d3d = Direct3DCreate9(D3D_SDK_VERSION);
	if (!d3d)
	{
		MessageBox(hWnd, "Error Initializing D3D Systems", "Error", MB_OK);
		return false;
	}

	D3DPRESENT_PARAMETERS d3dpp = { 0 };
	d3dpp.Windowed = !fullscreen;
	d3dpp.SwapEffect = D3DSWAPEFFECT_DISCARD;
	d3dpp.BackBufferFormat = D3DFMT_X8R8G8B8;
	d3dpp.BackBufferCount = 1;
	d3dpp.BackBufferWidth = game.WINDOWWIDTH;
	d3dpp.BackBufferHeight = game.WINDOWHEIGHT;
	d3dpp.hDeviceWindow = hWnd;

	d3d->CreateDevice(D3DADAPTER_DEFAULT,
		D3DDEVTYPE_HAL,
		hWnd,
		D3DCREATE_SOFTWARE_VERTEXPROCESSING,
		&d3dpp,
		&d3d_dev);
	if (!d3d_dev)
	{
		MessageBox(hWnd, "Failed to create Direct3D Device", "Error", MB_OK);
		return false;
	}

	//TODO(jzb): sort cursor showing out
	d3d_dev->ShowCursor(false);

	if (FAILED(d3d_dev->GetBackBuffer(0, 0, D3DBACKBUFFER_TYPE_MONO, &backbuffer)))
	{
		MessageBox(hWnd, "Failed to get backbuffer", "Error", MB_OK);
		return false;
	}

	if (FAILED(D3DXCreateFont(d3d_dev, WINDOWHEIGHT / 40, 0, FW_BOLD, 1, false, DEFAULT_CHARSET, OUT_DEFAULT_PRECIS, ANTIALIASED_QUALITY, DEFAULT_PITCH | FF_DONTCARE, "Arial", &font)))
	{
		MessageBox(hWnd, "Failed to create font", "Error", MB_OK);
		return false;
	}

	if (FAILED(D3DXCreateSprite(d3d_dev, &sprite)))
	{
		MessageBox(hWnd, "Failed to create sprite", "Error", MB_OK);
		return false;
	}


	return true;
}

bool CGame::Gamepad_Init(int controller_index)
{
	XINPUT_CAPABILITIES caps = { 0 };
	DWORD result = XInputGetCapabilities(controller_index, XINPUT_FLAG_GAMEPAD, &caps);
	if (caps.Type != 0) return false;
	return true;
}

std::string CGame::GetHardwareInfo()
{
	SYSTEM_INFO sys_info = { 0 };
	MEMORYSTATUSEX mem_info = { 0 };
	char processorName[128];
	std::string info_result;

	mem_info.dwLength = sizeof(mem_info);

	GetSystemInfo(&sys_info);
	GlobalMemoryStatusEx(&mem_info);

	// Get extended ids.
	int CPUInfo[4] = { -1 };
	__cpuid(CPUInfo, 0x80000000);
	unsigned int nExIds = CPUInfo[0];

	// Get the information associated with each extended ID.
	char CPUBrandString[0x40] = { 0 };
	for (unsigned int i = 0x80000000; i <= nExIds; ++i)
	{
		__cpuid(CPUInfo, i);

		// Interpret CPU brand string and cache information.
		if (i == 0x80000002)
		{
			memcpy(processorName,
				CPUInfo,
				sizeof(CPUInfo));
		}
		else if (i == 0x80000003)
		{
			memcpy(processorName + 16,
				CPUInfo,
				sizeof(CPUInfo));
		}
		else if (i == 0x80000004)
		{
			memcpy(processorName + 32, CPUInfo, sizeof(CPUInfo));
		}
	}

	char proc[150];
	sprintf_s(proc, "Processor:\n%s\n", processorName);
	char procCountBuffer[35];
	sprintf_s(procCountBuffer, "Num Processors: %d\n", sys_info.dwNumberOfProcessors);
	char memBuffer[35];
	sprintf_s(memBuffer, "System Mem: %llu GB\n", mem_info.ullTotalPhys / 1000000000);

	info_result.append(proc);
	info_result.append(procCountBuffer);
	info_result.append(memBuffer);

	return info_result;
}