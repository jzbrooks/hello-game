#pragma once
class Timer
{
	double PCFreq;
	double CounterStart;

public:
	Timer();
	~Timer();

	auto Reset() -> void;
	auto GetCounter() -> double;
	auto GetElapsedCounter() -> double;
};

