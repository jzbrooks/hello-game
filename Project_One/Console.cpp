#include "Console.h"
#include "win32_main.h"
#include "Game.h"

CConsole::CConsole()
{
}


CConsole::~CConsole()
{
}

bool CConsole::Init(CGame* game)
{
	bool succeeded = false;

	//todo(jzb): add more stuff here?

	UINT width = static_cast<UINT>(game->WINDOWWIDTH / 2.8f);
	UINT height = game->WINDOWHEIGHT / 4;
	game->d3d_dev->CreateOffscreenPlainSurface(width, height,
		D3DFMT_X8R8G8B8, D3DPOOL_DEFAULT, &background, nullptr);
	game->d3d_dev->ColorFill(background, nullptr, D3DCOLOR_XRGB(150, 150, 150));

	return true;
}

void CConsole::DrawIfShowing(CGame* game)
{
	RECT textboxbounds = { 0, 0, static_cast<long>(game->WINDOWWIDTH / 2.8f), game->WINDOWHEIGHT / 5 };

	if (m_bIsShowing)
	{
		char buffer[35];
		char fpsbuffer[35];

		game->d3d_dev->StretchRect(background, nullptr, game->backbuffer, &textboxbounds, D3DTEXF_NONE);
		game->DrawTextString(125, 0, "Debug Info");
		game->DrawTextString(0, 25, game->sHardware_Info.c_str());
		sprintf_s(buffer, "FrameTime:  %f  ms", game->timeDebugDelta/1000.0);
		game->DrawTextString(0, 110, buffer);
		sprintf_s(fpsbuffer, "FPS: %d", game->fps);
		game->DrawTextString(0, 130, fpsbuffer);
	}

}

bool CConsole::IsShowing()
{
	return m_bIsShowing;
}

void CConsole::Hide()
{
	m_bIsShowing = false;
}

void CConsole::Show()
{
	m_bIsShowing = true;
}