#pragma once
#include "Tile.h"
#include "Player.h"

class TileMap
{
public:
	int width = 40;
	int height = 15;

	LPDIRECT3DSURFACE9 alive_surface;
	LPDIRECT3DSURFACE9 dead_surface;

	std::vector<Tile> tilemap;

	TileMap();
	~TileMap();

	void Init(int, int);
	bool LoadMap(LPDIRECT3DDEVICE9, UINT[]);
	void Update(Player&, Camera& camera);
};

