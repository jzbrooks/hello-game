#include "Player.h"

using namespace std;

Player::Player()
{
}


Player::~Player()
{
}

bool Player::Init(LPDIRECT3DDEVICE9 d3d_dev)
{
	HRESULT hr = D3D_OK;

	this->SetLocation(100, 150);
	this->SetGrounded(false);
	
	hr = d3d_dev->CreateOffscreenPlainSurface(Player::width, Player::height,
											  D3DFMT_X8R8G8B8,
											  D3DPOOL_DEFAULT,
											  &this->surface,
											  nullptr);
	if (!SUCCEEDED(hr)) return false;

	hr = d3d_dev->ColorFill(this->surface, nullptr, D3DCOLOR_XRGB(255, 0, 0));

	return SUCCEEDED(hr);
}

void Player::Update(CGame* game, vector<Tile> collidables, int player_offset)
{
	grounded = false;

	for (auto &it : collidables)
	{
		RECT collidable = it.GetBounds();
		RECT player = bounds;
		RECT overlap;
		
		// apply player offset for being on nth screen
		player.left += player_offset;
		player.right += player_offset;
		
		ZeroMemory(&overlap, sizeof(overlap));
		if (IntersectRect(&overlap, &player, &collidable) && it.IsAlive())
		{
			long overlap_height = overlap.bottom - overlap.top;
			long overlap_width = overlap.right - overlap.left;
			long collidable_height = collidable.bottom - collidable.top;

			if (player.bottom - overlap_height == collidable.top && velocity.y > 0)
			{
				velocity.y = 0;
				SetLocation(bounds.left, collidable.top);
				jumping = false;
				grounded = true;
				continue;
			}
			
			if (player.right - collidable.left == overlap_width)
			{
				if (velocity.x > 0) velocity.x = 0;
				continue;
			}
			if (collidable.right - player.left == overlap_width)
			{
				if (velocity.x < 0) velocity.x = 0;
				continue;
			}

		}
	}
	

	if (jumping || !grounded)
		velocity.y += 1;
		
	
	this->Move(velocity);

}

bool Player::IsJumping() 
{ 
	return this->jumping; 
}

void Player::Jump()
{
	velocity.y = -15;
	this->jumping = true;
	this->grounded = false;
}

void Player::SetVelocity(Vector2 accel)
{
	this->velocity.x = accel.x;
	this->velocity.y = accel.y;
}

Vector2 Player::GetVelocity()
{
	return this->velocity;
}

void Player::SetLocation(long x, long y)
{
	this->bounds.top = y - this->height;
	this->bounds.bottom = y;
	this->bounds.left = x;
	this->bounds.right = x + this->width;
}

void Player::SetLocation(Vector2 location)
{
	this->bounds.top = location.y - this->height;
	this->bounds.bottom = location.y;
	this->bounds.left = location.x;
	this->bounds.right = location.x + this->width;
}

void Player::Move(Vector2 distance)
{
	Vector2 target_location;
	Vector2 curr_location = GetLocation();
	target_location.x = curr_location.x + distance.x;
	target_location.y = curr_location.y + distance.y;
	this->SetLocation(target_location);
}

Vector2 Player::GetLocation()
{
	return Vector2(this->bounds.left, this->bounds.bottom);
}

RECT* Player::GetBounds()
{
	return &this->bounds;
}

LPDIRECT3DSURFACE9 Player::GetSurface()
{
	return this->surface;
}

void Player::SetSurface(LPDIRECT3DSURFACE9 drawable)
{
	this->surface = drawable;
}

void Player::SetGrounded(bool onGround)
{
	this->grounded = onGround;
}

bool Player::IsGrounded()
{
	return this->grounded;
}
