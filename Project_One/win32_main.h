#pragma once

#pragma comment(lib, "d3d9.lib")
#pragma comment(lib, "d3dx9.lib")
#pragma comment(lib, "xinput.lib")


#include <Windows.h>
#include <d3d9.h>
#include <d3dx9.h>
#include <XInput.h>
#include <iostream>

#include "MainMenuState.h"
#include "LevelOneState.h"

static CGame game;

LRESULT CALLBACK WindowProc(HWND hWnd,
	UINT uMsg,
	WPARAM wParam,
	LPARAM lParam);

void Gamepad_Vibrate(int contNum = 0, int left = 65535, int right = 65535);
