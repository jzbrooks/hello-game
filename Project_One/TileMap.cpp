#include "win32_main.h"

TileMap::TileMap()
{
}

TileMap::~TileMap()
{
}

void TileMap::Init(int width, int height)
{	
	this->width = width;
	this->height = height;
}

bool TileMap::LoadMap(LPDIRECT3DDEVICE9 d3d_dev, UINT InitialState[])
{
	for (int row = 0; row < height; row++)
	{
		for (int col = 0; col < width; col++)
		{
			Tile curr_tile;
			RECT tile_bounds;
			tile_bounds.bottom = row*Tile::height + Tile::height;
			tile_bounds.top = row*Tile::height;
			tile_bounds.left = col*Tile::width;
			tile_bounds.right = col*Tile::width + Tile::width;

			d3d_dev->CreateOffscreenPlainSurface(Tile::width, Tile::height,
				D3DFMT_X8R8G8B8,
				D3DPOOL_DEFAULT,
				curr_tile.GetSurfacePtr(),
				nullptr);

			curr_tile.SetBounds(tile_bounds);

			if (InitialState[row*TileMap::width + col] == 1)
				curr_tile.SetAlive(d3d_dev, true);
			else
				curr_tile.SetAlive(d3d_dev, false);

			tilemap.push_back(curr_tile);
		}
	}

	return true;
}

void TileMap::Update(Player &player, Camera &camera)
{
	RECT playerBounds = *player.GetBounds();
	if (playerBounds.right > camera.right)
	{
		camera.left += game.WINDOWWIDTH;
		camera.right += game.WINDOWWIDTH;
		player.SetLocation(Vector2(0, playerBounds.bottom));
	}
	else if (playerBounds.left < 0)
	{
		camera.left -= game.WINDOWWIDTH;
		camera.right -= game.WINDOWWIDTH;
		player.SetLocation(Vector2(game.WINDOWWIDTH - Player::width, playerBounds.bottom));
	}
}
