#include "Tile.h"
#include "win32_main.h"

Tile::Tile()
{
}

Tile::~Tile()
{
}

void Tile::SetColor(LPDIRECT3DDEVICE9 d3d_dev, D3DCOLOR targ_color)
{
	this->color = targ_color;
	HRESULT hr = d3d_dev->ColorFill(this->surface, nullptr, targ_color);
	if (!SUCCEEDED(hr)) OutputDebugStringA("Color Set Failed");
}

bool Tile::IsAlive()
{
	return this->active;
}

void Tile::SetAlive(LPDIRECT3DDEVICE9 d3d_dev, bool alive)
{
	if (alive)
		this->SetColor(d3d_dev, D3DCOLOR_XRGB(50, 50, 50));
	else
		this->SetColor(d3d_dev, D3DCOLOR_XRGB(255, 255, 255));

	this->active = alive;
}

LPDIRECT3DSURFACE9 Tile::GetSurface()
{
	return this->surface;
}

void Tile::SetSurface(LPDIRECT3DSURFACE9 targ_surface)
{
	this->surface = targ_surface;
}

LPDIRECT3DSURFACE9* Tile::GetSurfacePtr()
{
	return &this->surface;
}

void Tile::SetLocation(long x, long y)
{
	this->bounds.bottom = y;
	this->bounds.top = y - this->height;
	this->bounds.left = x; 
	this->bounds.right = x + this->width;
}

RECT* Tile::GetBoundsPtr()
{
	return &this->bounds;
}

RECT Tile::GetBounds()
{
	return this->bounds;
}

void Tile::SetBounds(RECT bounds)
{
	this->bounds = bounds;
}