#include "MainMenuState.h"
#include "win32_main.h"

using namespace std;

CMainMenuState CMainMenuState::m_Instance;

void CMainMenuState::Init(CGame* game)
{
	current_selection = 0;
	options.push_back("Start");
	options.push_back("Options");
	options.push_back("Exit");

	stringColors.push_back(D3DCOLOR_XRGB(255, 0, 0));
	stringColors.push_back(D3DCOLOR_XRGB(255, 255, 255));
	stringColors.push_back(D3DCOLOR_XRGB(255, 255, 255));
}

void CMainMenuState::Cleanup()
{

}

void CMainMenuState::Pause()
{
}
void CMainMenuState::Resume()
{
}

void CMainMenuState::HandleEvents(CGame* game)
{
	XINPUT_STATE state = { 0 };

	for (int i = 0;
		i < 4;
		i++)
	{
		ZeroMemory(&state, sizeof(XINPUT_STATE));
		if (!XInputGetState(i, &state))
		{
			if (state.Gamepad.bLeftTrigger) {}

			if (state.Gamepad.bRightTrigger) {}

			if (state.Gamepad.wButtons & XINPUT_GAMEPAD_DPAD_UP)
			{
				game->console.Show();
			}
			if (state.Gamepad.wButtons & XINPUT_GAMEPAD_DPAD_DOWN)
			{
				game->console.Hide();
			}
			if (state.Gamepad.wButtons & XINPUT_GAMEPAD_DPAD_LEFT)
			{
			}
			if (state.Gamepad.wButtons & XINPUT_GAMEPAD_DPAD_RIGHT)
			{
			}
			if (state.Gamepad.wButtons & XINPUT_GAMEPAD_START) {}

			if (state.Gamepad.wButtons & XINPUT_GAMEPAD_BACK) { game->Quit(); }

			if (state.Gamepad.wButtons & XINPUT_GAMEPAD_LEFT_THUMB) {}

			if (state.Gamepad.wButtons & XINPUT_GAMEPAD_RIGHT_THUMB) {}

			if (state.Gamepad.wButtons & XINPUT_GAMEPAD_LEFT_SHOULDER) {}

			if (state.Gamepad.wButtons & XINPUT_GAMEPAD_RIGHT_SHOULDER) {}

			if (state.Gamepad.wButtons & XINPUT_GAMEPAD_A)
			{
				if (current_selection == 0)
					game->ChangeState(CLevelOneState::Instance());
				else if (current_selection == 1)
					//TODO(jzb): fill in
					;
				else if (current_selection == 2)
					game->Quit();
			}
			if (state.Gamepad.wButtons & XINPUT_GAMEPAD_B) {}

			if (state.Gamepad.wButtons & XINPUT_GAMEPAD_X) {}

			if (state.Gamepad.wButtons & XINPUT_GAMEPAD_Y) {}

			// handle controller joysticks
			if (state.Gamepad.sThumbLX < -XINPUT_GAMEPAD_LEFT_THUMB_DEADZONE)
			{
			}
			else if (state.Gamepad.sThumbLX > XINPUT_GAMEPAD_LEFT_THUMB_DEADZONE)
			{
			}
			else
			{
			}
			if (state.Gamepad.sThumbRX < 0)
			{
			}
			if (state.Gamepad.sThumbRX > 0)
			{
			}
			if (state.Gamepad.sThumbLY < -XINPUT_GAMEPAD_LEFT_THUMB_DEADZONE)
			{
				current_selection += (current_selection == 2) ? -2 : 1;
				Sleep(400);
			}
			if (state.Gamepad.sThumbLY > XINPUT_GAMEPAD_LEFT_THUMB_DEADZONE)
			{
				current_selection += (current_selection == 0) ? 2 : -1;
				Sleep(400);
			}
			if (state.Gamepad.sThumbRY < 0)
			{
			}
			if (state.Gamepad.sThumbRY > 0)
			{
			}

		}
		else
		{
			//NOTE: No controller connected. Log something?
			break;
		}
	}
}

void CMainMenuState::Update(CGame* game)
{
	for (UINT i = 0; i < stringColors.size(); i++)
	{
		if (i == current_selection)
			stringColors[i] = D3DCOLOR_XRGB(255, 0, 0);
		else
			stringColors[i] = D3DCOLOR_XRGB(255, 255, 255);
	}
}

void CMainMenuState::Draw(CGame* game)
{
	if (!game->d3d_dev) return;

	game->d3d_dev->Clear(0, nullptr, D3DCLEAR_TARGET, D3DCOLOR_XRGB(0, 0, 0), 1.0f, 0);

	if (game->d3d_dev->BeginScene())
	{
		game->DrawTextString(100, 100, options[0].c_str(), stringColors[0]);
		game->DrawTextString(100, 200, options[1].c_str(), stringColors[1]);
		game->DrawTextString(100, 300, options[2].c_str(), stringColors[2]);

		game->d3d_dev->EndScene();

		game->d3d_dev->Present(nullptr, nullptr,
			nullptr, nullptr);
	}

}