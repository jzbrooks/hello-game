#pragma once
#include "d3d9.h"
class CGame;

class CConsole
{
	
	LPDIRECT3DSURFACE9 background;

	bool m_bIsShowing;
public:
	CConsole();
	~CConsole();
	bool Init(CGame*);

	void DrawIfShowing(CGame*);
	bool IsShowing();
	void Hide();
	void Show();
};

