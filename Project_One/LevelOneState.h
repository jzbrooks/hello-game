#pragma once
#include "GameState.h"
#include "LevelState.h"
#include "Player.h"
#include "TileMap.h"
#include "Console.h"

class CLevelOneState :
	public CGameState,
	public LevelState
{
public:

	virtual void Init( CGame* game );
	virtual void Cleanup();

	virtual void Pause();
	virtual void Resume();

	virtual void HandleEvents(CGame* game);
	virtual void Update(CGame* game);
	virtual void Draw(CGame* game);

	static CLevelOneState* Instance()
	{
		return &m_Instance;
	}

private:
	static CLevelOneState m_Instance;
	
	Player playercube;
	TileMap map;
};

