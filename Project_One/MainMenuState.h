#pragma once
#include "GameState.h"
#include <string>

class CMainMenuState : 
	public CGameState
{
public:

	virtual void Init(CGame* game);
	virtual void Cleanup();

	virtual void Pause();
	virtual void Resume();

	virtual void HandleEvents(CGame* game);
	virtual void Update(CGame* game);
	virtual void Draw(CGame* game);

	static CMainMenuState* Instance()
	{
		return &m_Instance;
	}

private:
	static CMainMenuState m_Instance;

	std::vector<std::string> options;
	std::vector<DWORD>		 stringColors;
	int current_selection;
};

