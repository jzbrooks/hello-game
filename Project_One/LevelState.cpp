#include "LevelState.h"
#include <XInput.h>
auto LevelState::HandlePlayerInput(CGame* game, Player& player) -> void
{
	XINPUT_STATE state = { 0 };

	for (int i = 0;
		i < 4;
		i++)
	{
		Vector2 player_vel = {};

		ZeroMemory(&state, sizeof(XINPUT_STATE));
		if (!XInputGetState(i, &state))
		{
			if (state.Gamepad.bLeftTrigger) {}

			if (state.Gamepad.bRightTrigger) {}

			if (state.Gamepad.wButtons & XINPUT_GAMEPAD_DPAD_UP)
			{
				game->console.Show();
			}
			if (state.Gamepad.wButtons & XINPUT_GAMEPAD_DPAD_DOWN)
			{
				game->console.Hide();
			}
			if (state.Gamepad.wButtons & XINPUT_GAMEPAD_DPAD_LEFT)
			{
			}
			if (state.Gamepad.wButtons & XINPUT_GAMEPAD_DPAD_RIGHT)
			{
			}
			if (state.Gamepad.wButtons & XINPUT_GAMEPAD_START) {}

			if (state.Gamepad.wButtons & XINPUT_GAMEPAD_BACK) { game->Quit(); }

			if (state.Gamepad.wButtons & XINPUT_GAMEPAD_LEFT_THUMB) {}

			if (state.Gamepad.wButtons & XINPUT_GAMEPAD_RIGHT_THUMB) {}

			if (state.Gamepad.wButtons & XINPUT_GAMEPAD_LEFT_SHOULDER) {}

			if (state.Gamepad.wButtons & XINPUT_GAMEPAD_RIGHT_SHOULDER) {}

			if (state.Gamepad.wButtons & XINPUT_GAMEPAD_A)
			{
				if (!player.IsJumping() && player.IsGrounded()) player.Jump();
			}
			if (state.Gamepad.wButtons & XINPUT_GAMEPAD_B) {}

			if (state.Gamepad.wButtons & XINPUT_GAMEPAD_X) {}

			if (state.Gamepad.wButtons & XINPUT_GAMEPAD_Y) {}

			// handle controller joysticks
			if (state.Gamepad.sThumbLX < -XINPUT_GAMEPAD_LEFT_THUMB_DEADZONE)
			{
				player_vel.x = static_cast<long>(-0.5 * game->timeTotalDelta * 1000.0);
			}
			else if (state.Gamepad.sThumbLX > XINPUT_GAMEPAD_LEFT_THUMB_DEADZONE)
			{
				player_vel.x = static_cast<long>(0.5 * game->timeTotalDelta * 1000.0);
			}
			else
			{
				player_vel.x = 0;
			}
			if (state.Gamepad.sThumbRX < 0)
			{
			}
			if (state.Gamepad.sThumbRX > 0)
			{
			}
			if (state.Gamepad.sThumbLY < 0)
			{
			}
			if (state.Gamepad.sThumbLY > 0)
			{
			}
			if (state.Gamepad.sThumbRY < 0)
			{
			}
			if (state.Gamepad.sThumbRY > 0)
			{
			}

			Vector2 curr_vel = player.GetVelocity();
			player_vel.y = curr_vel.y;
			player.SetVelocity(player_vel);
		}
		else
		{
			//NOTE: No controller connected. Log something?
			break;
		}
	}
}

auto LevelState::RenderLevel(CGame* game, TileMap& map, Player& player) -> void
{
	if (!game->d3d_dev) return;

	game->d3d_dev->Clear(0, nullptr, D3DCLEAR_TARGET, D3DCOLOR_XRGB(0, 0, 0), 1.0f, 0);

	if (game->d3d_dev->BeginScene())
	{
		for (auto &it : map.tilemap)
		{
			RECT MapView;
			MapView.top = it.GetBounds().top - game->camera.top;
			MapView.bottom = MapView.top + Tile::height;
			MapView.left = it.GetBounds().left - game->camera.left;
			MapView.right = MapView.left + Tile::width;
			game->d3d_dev->StretchRect(it.GetSurface(), nullptr, game->backbuffer, &MapView, D3DTEXF_NONE);
		}

		game->d3d_dev->StretchRect(player.GetSurface(), nullptr, game->backbuffer, player.GetBounds(), D3DTEXF_NONE);

		game->console.DrawIfShowing(game);

		game->d3d_dev->EndScene();

		game->d3d_dev->Present(nullptr, nullptr,
			nullptr, nullptr);
	}

}