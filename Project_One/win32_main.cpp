#include <thread>
#include <chrono>

#include "win32_main.h"
#include "Timer.h"

LRESULT CALLBACK
WindowProc(HWND hWnd,
		   UINT uMsg,
		   WPARAM wParam,
		   LPARAM lParam)
{
	switch (uMsg)
	{
	case WM_QUIT:
	case WM_DESTROY:
		game.Quit();
	}
	return DefWindowProc(hWnd, uMsg, wParam, lParam);
}

int CALLBACK
WinMain(HINSTANCE Instance,
		HINSTANCE PrevInstance,
		LPSTR CmdLine,
		int CmdShow)
{

	game.Init(Instance, CmdShow, "PLACEHOLDER", false);
	game.ChangeState( CMainMenuState::Instance() );

	double targetFPS = 60.0;
	double targetMicroseconds = double(1.0 / targetFPS) * 1000000.0;
	MSG msg;
	int framecounter = 0;
	Timer frameTimer;
	while (game.IsRunning())
	{
		frameTimer.Reset();

		PeekMessage(&msg, nullptr, 0, 0, PM_REMOVE);
		TranslateMessage(&msg);
		DispatchMessage(&msg);

		game.HandleEvents();
		game.Update();
		game.Draw();

		game.timeActualDelta = frameTimer.GetElapsedCounter();

		DWORD secondsToSleep = static_cast<DWORD>(targetMicroseconds - game.timeActualDelta);
		
		if (game.timeActualDelta < targetMicroseconds)
		{
			double deltaSeconds = (game.timeActualDelta + static_cast<double>(secondsToSleep)) / 1000000.0;
			game.fps = static_cast<DWORD>(1.0 / deltaSeconds);
			game.timeTotalDelta = (game.timeActualDelta + secondsToSleep) / 1000000.0;
			std::this_thread::sleep_for(std::chrono::microseconds(secondsToSleep));
		}
		else
		{
			game.timeTotalDelta = game.timeActualDelta;
			double deltaSeconds = game.timeActualDelta / 1000000.0;
			game.fps = static_cast<DWORD>(1.0 / deltaSeconds);
		}

		if (++framecounter % 180 == 0)
		{
			game.timeDebugDelta = game.timeActualDelta;
			framecounter = 0;
		}

	}

	game.Cleanup();
	return (int)msg.wParam;
}


void Gamepad_Vibrate(int contNum, int left, int right)
{
	XINPUT_VIBRATION vibs = { 0 };
	vibs.wLeftMotorSpeed = left;
	vibs.wRightMotorSpeed = right;
	XInputSetState(contNum, &vibs);
}
