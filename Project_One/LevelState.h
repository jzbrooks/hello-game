#pragma once

#include "Game.h"
#include "TileMap.h"
#include "Player.h"

class LevelState
{
protected:
	auto HandlePlayerInput(CGame* game, Player& player) -> void;

	auto RenderLevel(CGame* game, TileMap& map, Player& player) -> void;
};



