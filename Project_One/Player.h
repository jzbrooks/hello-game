#pragma once
#include <d3d9.h>
#include <vector>
#include "Vector2.h"
#include "Tile.h"
#include "Game.h"

class Player
{
	LPDIRECT3DSURFACE9 surface;
	RECT bounds;
	Vector2 velocity;

	bool jumping;
	bool grounded;

public:
	static const int width = 50;
	static const int height = 75;
	
	Player();
	~Player();

	RECT* GetBounds();
	
	LPDIRECT3DSURFACE9 GetSurface();
	void SetSurface(LPDIRECT3DSURFACE9);

	Vector2 GetLocation();
	void SetLocation(long x, long y);
	void SetLocation(Vector2 location);
	
	void Move(Vector2 distance);

	void SetVelocity(Vector2 location);
	Vector2 GetVelocity();

	void Jump();
	bool IsJumping();
	
	void SetGrounded(bool grounded);
	bool IsGrounded();

	bool Init(LPDIRECT3DDEVICE9);

	void Update(CGame* game, std::vector<Tile> collidables, int player_offset);
};

