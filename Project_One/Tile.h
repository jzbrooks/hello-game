#pragma once
#include "d3d9.h"

class Tile
{
	RECT bounds;
	LPDIRECT3DSURFACE9 surface;
	D3DCOLOR color;

	bool active;
public:
	static const long height = 50;
	static const long width = 50;
	
	Tile();
	~Tile();

	void SetColor(LPDIRECT3DDEVICE9, D3DCOLOR);

	bool IsAlive();
	void SetAlive(LPDIRECT3DDEVICE9 d3d_dev, bool alive);
	
	LPDIRECT3DSURFACE9 GetSurface();
	LPDIRECT3DSURFACE9* GetSurfacePtr();
	void SetSurface(LPDIRECT3DSURFACE9);

	RECT* GetBoundsPtr();
	RECT GetBounds();
	void SetBounds(RECT);

	void SetLocation(long, long);
};

