#include "Timer.h"
#include "win32_main.h"

Timer::Timer() : CounterStart(0)
{
	LARGE_INTEGER li;
	if (!QueryPerformanceFrequency(&li))
		OutputDebugString("Cannot get freq");

	PCFreq = static_cast<double>(li.QuadPart) / 1000000.0;
}

Timer::~Timer() {}

auto Timer::GetCounter() -> double
{
	LARGE_INTEGER li;
	QueryPerformanceCounter(&li);
	return double(li.QuadPart) / PCFreq;
}

auto Timer::GetElapsedCounter() -> double
{
	double currentTime = GetCounter();
	double elapsedTime = currentTime - CounterStart;
	return elapsedTime;
}

auto Timer::Reset() -> void
{
	LARGE_INTEGER li;
	QueryPerformanceCounter(&li);
	CounterStart = static_cast<double>(li.QuadPart) / PCFreq;
}