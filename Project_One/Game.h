#pragma once

#include "Windows.h"
#include "d3d9.h"
#include "d3dx9.h"
#include <string>
#include <vector>

#include "Console.h"

class CGameState;

typedef RECT Camera;

class CGame
{
public:
	bool Init( HINSTANCE Instance, int CmdShow, std::string Title, bool fullscreen );
	void Cleanup();

	void ChangeState( CGameState* state );
	void PushState( CGameState* state );
	void PopState();

	void HandleEvents();
	void Update();
	void Draw();

	bool IsRunning() { return m_running; }
	void Quit() { m_running = false; }

	void DrawTextString(int x, int y, const char * text, DWORD color = D3DCOLOR_XRGB(0,0,0));

	std::string sHardware_Info;

	LPDIRECT3D9    d3d;
	LPDIRECT3DDEVICE9 d3d_dev;
	LPDIRECT3DSURFACE9 backbuffer;
	LPD3DXSPRITE   sprite;
	LPD3DXFONT     font;
	
	double timeLast;
	double timeNow;
	double timeTotalDelta;
	double timeActualDelta;
	double timeDebugDelta;
	long   fps;

	Camera camera;
	
	CConsole console;

	int WINDOWHEIGHT = 768, WINDOWWIDTH = 1024;

private:
	std::vector<CGameState*> states;

	std::string GetHardwareInfo();

	bool D3D_Init( HWND, BOOL );
	bool Gamepad_Init( int controller_index = 0 );
	bool m_running;
};

